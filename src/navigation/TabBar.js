import React from 'react';

import { View, Text, TouchableOpacity, Dimensions, Image } from 'react-native';
import { BoxShadow } from "react-native-shadow";
import ImageLoader from "../helpers/ImagesLoader";

const { width, height } = Dimensions.get('window');

export default TabBar = ({ state, descriptors, navigation }) => {
    const focusedOptions = descriptors[state.routes[state.index].key].options;

    if (focusedOptions.tabBarVisible === false) {
        return null;
    }
    const shadowOpt = {
        width: width,
        height: 45,
        color: "#000",
        border: 10,
        radius: 1,
        opacity: 0.05,
        x: 0,
        y: 0,
        style: { marginVertical: 0 },
    };


    return (
        <BoxShadow setting={shadowOpt}>
            <View style={{ flexDirection: 'row', height: 45, width: '100%', alignSelf: 'center', borderTopLeftRadius: 30, }}>
                {state.routes.map((route, index) => {
                    const { options } = descriptors[route.key];
                    const label =
                        options.tabBarLabel !== undefined
                            ? options.tabBarLabel
                            : options.title !== undefined
                                ? options.title
                                : route.name;

                    const isFocused = state.index === index;

                    const onPress = () => {
                        const event = navigation.emit({
                            type: 'tabPress',
                            target: route.key,
                            canPreventDefault: true,
                        });

                        if (!isFocused && !event.defaultPrevented) {
                            navigation.navigate(route.name);
                        }
                    };

                    const onLongPress = () => {
                        navigation.emit({
                            type: 'tabLongPress',
                            target: route.key,
                        });
                    };
                    const icon = () => {
                        if (index == 0) return 'home_icon';
                        if (index == 1) return 'pray_icon';
                        if (index == 2) return 'broad_icon';
                        if (index == 3) return 'help_icon';
                        if (index == 4) return 'profile_icon';
                    }
                    return (
                        <TouchableOpacity
                            key={index}
                            accessibilityRole="button"
                            accessibilityStates={isFocused ? ['selected'] : []}
                            accessibilityLabel={options.tabBarAccessibilityLabel}
                            testID={options.tabBarTestID}
                            onPress={onPress}
                            onLongPress={onLongPress}
                            style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: 'white', justifyContent: 'space-around', paddingTop: 5 }}
                        >
                            <View style={{ width: 20 }}>
                                <Image source={ImageLoader(icon())} style={{ width: index == 2 ? 20.36 : 20, height: index == 2 ? 13.42 : 20, tintColor: isFocused ? '#0D7367' : '#979DA0' }} />

                            </View>
                            <Text style={{ color: isFocused ? '#0D7367' : '#979DA0', fontFamily: 'Nunito-Medium', fontSize: 9 }}>
                                {label}
                            </Text>
                        </TouchableOpacity>
                    );
                })}
            </View>
        </BoxShadow>
    );
}

