import React, { useEffect, useMemo, useState } from 'react';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import TabBar from './TabBar';

import Register from '../screens/Auth/Register';
import Login from '../screens/Auth/Login';
import Home from '../screens/Home/Home';
import Familly from '../screens/Familly/Familly';

import Pray from '../screens/Pray/Pray';
import Broadcast from '../screens/Broadcast/Broadcast';
import Helps from '../screens/Helps/Helps';
import Profile from '../screens/Profile/Profile';
import DetailEvent from '../screens/Order/DetailEvent';
import ListDetail from '../screens/Order/ListDetail';





import Splash from '../screens/Splash';
import { useSelector } from 'react-redux';
import OnBoarding from '../screens/OnBoarding/OnBoarding';


const AuthStack = createStackNavigator();

const Tabs = createBottomTabNavigator();
const HomeStack = createStackNavigator();
const PrayStack = createStackNavigator();
const BroadcastStack = createStackNavigator();
const HelpsStack = createStackNavigator();
const ProfileStack = createStackNavigator();


const HomeStackScreen = () => (
    <HomeStack.Navigator screenOptions={{ headerShown: false }} >
        <HomeStack.Screen name='Home' component={Home} />
        <HomeStack.Screen name='Familly' component={Familly} />
        <HomeStack.Screen name='DetailEvent' component={DetailEvent} />
        <HomeStack.Screen name='ListDetail' component={ListDetail} />


    </HomeStack.Navigator>
)
const PrayStackScreen = () => (
    <PrayStack.Navigator screenOptions={{ headerShown: false }} >
        <PrayStack.Screen name='Pray' component={Pray} />
    </PrayStack.Navigator>
)

const BroadcastStackScreen = () => (
    <BroadcastStack.Navigator screenOptions={{ headerShown: false }} >
        <BroadcastStack.Screen name='Broadcast' component={Broadcast} />
    </BroadcastStack.Navigator>
)
const HelpsStackScreen = () => (
    <HelpsStack.Navigator screenOptions={{ headerShown: false }} >
        <HelpsStack.Screen name='Helps' component={Helps} />
    </HelpsStack.Navigator>
)
const ProfileStackScreen = () => (
    <ProfileStack.Navigator screenOptions={{ headerShown: false }} >
        <ProfileStack.Screen name='Profile' component={Profile} />
    </ProfileStack.Navigator>
)


const TabScreen = () => {
    const getTabBarVisibility = (route) => {
        try {
            const routeName = route.state
                ? route.state.routes[route.state.index].name
                : '';
            console.log(route.state.index)

            if (route.state.index == 0) {
                return true;
            } else {
                return false;
            }
        } catch (error) {
            return true;
        }
    }
    return (
        <Tabs.Navigator tabBar={props => <TabBar {...props} />}>
            <Tabs.Screen name='Beranda' component={HomeStackScreen} options={({ route }) => ({
                tabBarVisible: getTabBarVisibility(route)
            })} />
            <Tabs.Screen name='Doa' component={PrayStackScreen} />
            <Tabs.Screen name='Siaran' component={BroadcastStackScreen} />
            <Tabs.Screen name='Bantuan' component={HelpsStackScreen} />
            <Tabs.Screen name='Profil' component={ProfileStackScreen} />

        </Tabs.Navigator>)
};
const NavigationApps = () => {
    const [isLoading, setIsLoading] = useState(true);
    const mainState = useSelector((state) => state.reducerMain);

    console.log(mainState)
    useEffect(() => {
        setTimeout(() => {
            setIsLoading(false)
        }, 2000)
    }, [])
    if (isLoading) {
        return <Splash />
    }
    return (
        <NavigationContainer>
            {mainState.apiToken == '' ? (
                <AuthStack.Navigator screenOptions={{ headerShown: false }} >
                    <AuthStack.Screen name='OnBoarding' component={OnBoarding} options={{ title: 'Register' }} />
                    <AuthStack.Screen name='Register' component={Register} options={{ title: 'Register' }} />
                    <AuthStack.Screen name='Login' component={Login} options={{ title: 'Login' }} />
                    <AuthStack.Screen name='Home' component={Home} options={{ title: 'Home' }} />
                </AuthStack.Navigator>) :
                TabScreen()
            }
        </NavigationContainer>
    )
}
export default NavigationApps