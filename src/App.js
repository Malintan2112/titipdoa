/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import { Provider } from 'react-redux';

import { Root } from "native-base";
import { codePushConfigure, setGlobalFontFamily } from './helpers/MainHelpers';
import NavigationApps from './navigation';
import store from './redux/store';


const App: () => React$Node = () => {
  useEffect(() => {
    // Configuring Code Push 
    codePushConfigure
    setGlobalFontFamily()
  })

  return (
    <>
      <Provider store={store}>
        <Root>
          <StatusBar translucent backgroundColor="transparent" barStyle="dark-content" />
          <NavigationApps />
        </Root>
      </Provider>
    </>
  );
};


export default App;
