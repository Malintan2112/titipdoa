import codePush from 'react-native-code-push';
import {
    Alert,
} from 'react-native';
import { setCustomText } from 'react-native-global-props';

export const setGlobalFontFamily = () => {
    const customTextProps = {
        allowFontScaling: false,
        style: {
            fontFamily: 'Nunito-Regular',
        }
    };
    console.log('gol')
    setCustomText(customTextProps);
}

const syncStatus = (status) => {
    switch (status) {
        case codePush.SyncStatus.CHECKING_FOR_UPDATE:
            console.log("checking for update")
            break;
        case codePush.SyncStatus.DOWNLOADING_PACKAGE:
            console.log("downloading package")
            break;
        case codePush.SyncStatus.UP_TO_DATE:
            console.log('up to date')
            break;
        case codePush.SyncStatus.INSTALLING_UPDATE:
            console.log('instaling update ');
            break;
        case codePush.SyncStatus.UPDATE_INSTALLED:
            // Alert.alert("Notification", "Update Instaled")
            break;
        case codePush.SyncStatus.AWAITING_USER_ACTION:
            console.log('awaiting user action');
            break;
        default:
            break;
    }
}

export const codePushConfigure = codePush.sync({
    updateDialog: false,
    installMode: codePush.InstallMode.IMMEDIATE
}, syncStatus)