import * as Hooks from "./Hooks";
import * as Session from "./Session";

// import lang from "./Language";
import { createRequest } from "./Http";

// import ImagePicker from "react-native-image-crop-picker";

// import { ActionSheet } from "native-base";

const TAG = "Image Loader Lib";

// NOTE Please put image path inside imageList
const imageList = {

  //OnBoarding
  ob1: require("../assets/images/onBoarding/ob1.png"),
  ob2: require("../assets/images/onBoarding/ob2.png"),




  // Login
  logo: require("../assets/images/logo.png"),
  google: require("../assets/images/google.png"),
  facebook: require("../assets/images/facebook.png"),
  login_background: require("../assets/images/login_background.png"),
  mail: require("../assets/images/mail.png"),
  key: require("../assets/images/key.png"),
  eye: require("../assets/images/eye.png"),
  user_form: require("../assets/images/user_form.png"),
  phone: require("../assets/images/phone.png"),

  // Home 
  background: require("../assets/images/background.png"),
  tahlil_categori: require("../assets/images/tahlil_categori.png"),
  yasin_categori: require("../assets/images/yasin_categori.png"),
  quran_categori: require("../assets/images/quran_categori.png"),
  icon_tahlil: require("../assets/images/icon_tahlil.png"),
  icon_yasin: require("../assets/images/icon_yasin.png"),
  icon_quran: require("../assets/images/icon_quran.png"),
  icon_like: require("../assets/images/icon_like.png"),
  icon_share: require("../assets/images/icon_share.png"),
  icon_bookmark: require("../assets/images/icon_bookmark.png"),
  icon_calendar: require("../assets/images/icon_calendar.png"),
  icon_clock: require("../assets/images/icon_clock.png"),
  icon_location: require("../assets/images/icon_location.png"),
  icon_notif: require("../assets/images/icon_notif.png"),


  // Footer
  home_icon: require("../assets/images/home_icon.png"),
  help_icon: require("../assets/images/help_icon.png"),
  profile_icon: require("../assets/images/profile_icon.png"),
  pray_icon: require("../assets/images/pray_icon.png"),
  broad_icon: require("../assets/images/broad_icon.png"),
  icon_arrow_back: require("../assets/images/icon_arrow_back.png"),
  edit: require("../assets/images/edit.png"),
  check_box_non: require("../assets/images/check_box_non.png"),
  check_box_aktive: require("../assets/images/check_box_aktive.png"),
  add_circle: require("../assets/images/add_circle.png"),








  /**  **/
};

function fetchObject(prop) {
  // split prop
  var _index = prop.indexOf(".");

  // prop split found
  if (_index > -1) {
    // re-execute this function to get property inside other property
    return fetchObject(prop.substr(_index + 1));
  }

  return imageList[prop];
}

export default function loader(index, default_type = "profile") {
  let isHaveSlash = /\//.test(index);
  let isFullURL = /http/.test(index);

  if (!index || (isHaveSlash && !isFullURL)) {
    return fetchObject(`${default_type}_default`); // as Default Image
  } else if (isFullURL) {
    return { uri: index };
  } else {
    let imgValue = fetchObject(index);
    return imgValue;
  }
}

/**
 * Select & Upload Image
 * @param {Enum} type - single | multi
 * @param {Object} data - request data
 * @return {Promise}
 */
export function selectImage(type = "single", data = {}, mode = "picker") {
  let pickerSource = [lang("btn.gallery"), lang("btn.camera"), lang("btn.cancel")];
  let cancelButtonIndex = 2;
  let cameraButtonIndex = 1;
  let galeryButtonIndex = 0;

  return new Promise((resolve, reject) => {
    let imagePickerOptions = {};
    switch (type) {
      case "single":
        imagePickerOptions = {
          width: 300,
          height: 300,
          cropping: true,
        };
        break;
      case "multi":
        imagePickerOptions = { multiple: true };
        break;
      default:
        type = "single";
        imagePickerOptions = {
          width: 300,
          height: 300,
          cropping: true,
        };
        break;
    }

    // if (Object.keys(data).length > 0) {
    // Select Source
    // ActionSheet.show(
    //   {
    //     title: lang("title.selectImageSource"),
    //     options: pickerSource,
    //     cancelButtonIndex,
    //   },
    //   (buttonIndex) => {
    //     if (buttonIndex === galeryButtonIndex) {
    //       // Image Source from Galery
    //       // Open Image Picker
    //       ImagePicker.openPicker(imagePickerOptions)
    //         .then((images) => {
    //           Hooks.consoleLog("Image Picked", images);
    //           //resolve(images);
    //           imageUploader(type, data, images)
    //             .then((response) => {
    //               resolve(response);
    //             })
    //             .catch((err) => {
    //               reject({
    //                 message: `Image Uploader Error: ${err.message}`,
    //               });
    //             });
    //         })
    //         .catch((err) => {
    //           reject({
    //             message: `Image Picker Error: ${err.message}`,
    //           });
    //         });
    //     } else if (buttonIndex === cameraButtonIndex) {
    //       // Image Source from Camera
    //       // Open Image Picker
    //       ImagePicker.openCamera(imagePickerOptions)
    //         .then((images) => {
    //           Hooks.consoleLog("Image Picked", images);
    //           // resolve(images);
    //           imageUploader(type, data, images)
    //             .then((response) => {
    //               resolve(response);
    //             })
    //             .catch((err) => {
    //               reject({
    //                 message: `Image Uploader Error: ${err.message}`,
    //               });
    //             });
    //         })
    //         .catch((err) => {
    //           reject({
    //             message: `Image Picker Error: ${err.message}`,
    //           });
    //         });
    //     }
    //   }
    // );

    // } else {
    //   reject({ message: lang('error.upload_data') });
    // }
  });
}

/**
 * Image Uploader
 *
 * @param {Enum} type - multi | single
 * @param {Object} data - request data
 * @param {Object} images - Data from Image Picker
 *
 * @return {Promise} Response from API
 */
async function imageUploader(type, data, images) {
  let alternateFilename;
  let image_data;

  // Prepare image data (single | multi)
  if (type == "multi") {
    image_data = [];
    for (var i = 0; i < images.length; i++) {
      let split_path = images[i].path.split("/");
      alternateFilename = split_path[split_path.length - 1];

      image_data[i] = {
        uri: images[i].path,
        type: images[i].mime,
        file: images[i].filename || alternateFilename,
      };
    }
  } else if (type == "single") {
    let split_path = images.path.split("/");
    alternateFilename = split_path[split_path.length - 1];
    image_data = {
      uri: images.path,
      type: images.mime,
      name: images.filename || alternateFilename,
    };
  }
  // Prepare Request Data
  data.photo = image_data;
  // data.name = image_data.name;
  // data.client_id = Session.getValue(Session.TYPE_CLIENT);
  // data.user_id = Session.getValue(Session.USER_ID);
  let request = {
    // link: 'api/test/image',
    // link: "api/profile/photo",
    custom_link: 'https://www.ayoohris.id/main/api/profile/change_photo',
    method: "POST",
    headers: {
      Accept: "application/json",
      // set content type into form-data, so Http Helper will transform the data into FormData instead of JSON
      "Content-Type": "multipart/form-data",
      Authorization: Session.getValue(Session.API_TOKEN),
    },
    data,
  };
  console.log("image_data", request);

  let response = await createRequest(request);

  return response;
}
