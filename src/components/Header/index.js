import React, { Component } from "react";
// import PropTypes from 'prop-types';

import {
    Alert,
    ActivityIndicator,
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
    ImageBackground,
    RefreshControl,
    Keyboard,
    StatusBar,
    Dimensions,
} from "react-native";

import {
    Button,
    Text,
    Header,
    Content,
    Container,
    InputGroup,
    Form,
    Label,
    Card,
    //Icon,
    Body,
    Input,
    Item,
    Left,
    Right,
    Title,
    CardItem,
    List,
    ListItem,
} from "native-base";

import ImageLoader from "../../helpers/ImagesLoader";


import LinearGradient from "react-native-linear-gradient";


const { width, height } = Dimensions.get("window");

export default class HeaderItems extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showLogo: false,
            countdown: 3,
            seconds: 5,
        };
    }

    render = () => {
        const { back, title, navigation, aim, customIcon } = this.props;
        return (
            <View style={[{ position: "relative", marginTop: Platform.OS != "android" ? -5 : 0, minHeight: 60, height: 80, }]}>
                <View
                    style={{
                        flex: 1,
                        resizeMode: "cover",
                        justifyContent: "center",
                        backgroundColor: '#0D7367'
                    }}
                >
                    <Header
                        transparent
                        style={([{ position: "relative" }])}
                    >
                        <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

                        <View style={{}}>
                            {back == true ? (
                                <TouchableOpacity
                                    onPress={() => navigation.goBack()}
                                    style={{
                                        justifyContent: "center",
                                        alingItems: "center",
                                        width: width * 0.2,
                                        marginTop: Platform.OS != "android" ? -6 : 0,
                                        height: 60,
                                    }}
                                >
                                    <Image
                                        source={ImageLoader("icon_arrow_back")}
                                        resizeMode="contain"
                                        style={{ width: 15, height: 15, alignSelf: "center" }}
                                    />
                                </TouchableOpacity>
                            ) : <TouchableOpacity

                                style={{
                                    justifyContent: "center",
                                    alingItems: "center",
                                    width: width * 0.2,
                                    marginTop: Platform.OS != "android" ? -6 : 0,
                                    height: 60,
                                }}
                            >
                                    <Image
                                        source={ImageLoader("icon_arrow_back")}
                                        resizeMode="contain"
                                        style={{ width: 15, height: 15, alignSelf: "center", opacity: 0 }}

                                    />
                                </TouchableOpacity>}
                        </View>
                        <View
                            style={{
                                width: width * 0.6,
                                alingItems: "center",
                                justifyContent: "center",
                            }}
                        >
                            <Title style={{ fontSize: 15, fontFamily: "Nunito-SemiBold", textAlign: "center", color: "white" }}>
                                {title}
                            </Title>
                        </View>
                        <View style={{ width: width * 0.2 }} />
                    </Header>
                </View>
            </View>
        );
    };
}
