import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions,
    ActivityIndicator,
    Animated
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';

import ImageLoader from "../../helpers/ImagesLoader";
import { Card } from 'native-base';
const { width, height } = Dimensions.get('window');

export default Home = (props) => {
    const [scrollY] = useState(new Animated.Value(0))
    return (
        <View style={styles.MainContainer}>
            <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

            <ScrollView
                onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], { useNativeDriver: false })}
            >

                <View style={{ height: 1000 }}>
                    <Image source={ImageLoader('icon_notif')} style={{ position: "absolute", width: 19, height: 23, top: 50, right: 30, zIndex: 1 }} />

                    <Animatable.View
                        animation="fadeInDown"
                        duration={1000}
                    >
                        <Animated.Image source={ImageLoader('background')} style={{
                            width, height: 205, transform: [{
                                translateY: scrollY.interpolate({
                                    inputRange: [0, 10],
                                    outputRange: [0, 10]
                                })
                            }]
                        }} />
                    </Animatable.View>
                    <View style={{ width, height: 50, backgroundColor: 'white' }}>

                    </View>
                    <Animatable.View
                        animation="slideInUp"
                        duration={1000}
                        style={{ width, height: '100%', position: 'absolute' }}>
                        <View style={{ marginTop: 75, width, height: 175, alignItems: 'center' }}>
                            <View style={{ width: width * 0.9, height: 50, flexDirection: "row", alignItems: 'center' }}>
                                <View style={{ backgroundColor: 'white', borderRadius: 25, height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                                    <Text style={{ fontSize: 16, fontFamily: 'Nunito-Bold', color: '#0D7367' }}>AT</Text>
                                </View>

                                <View style={{ marginLeft: 10, width: width * 0.6 }}>
                                    <Text style={{ fontSize: 14, fontFamily: 'Nunito-Bold', color: 'white' }}>
                                        Ahmad Tedjo
                                     </Text>
                                    <Text style={{ fontSize: 11, color: 'white' }}>
                                        Tegalsari, Candisar, Semarang, Jawa Tengah
                                     </Text>
                                </View>
                            </View>
                            <Card style={{ width: width * 0.9, height: 82, marginTop: 40, borderRadius: 5 }}>
                                <View style={styles.containerCount}>
                                    <View style={styles.containCount}>
                                        <View style={styles.containerTitleCount}>
                                            <Image source={ImageLoader('icon_tahlil')} style={{ width: 14, height: 9, marginRight: 5 }} />
                                            <Text style={styles.titleCount}>Tahlil</Text>
                                        </View>
                                        <Text style={styles.valueCount}>
                                            4
                                        </Text>
                                        <Text style={styles.dateCount}>
                                            Terakhir ikut 29/10/2020
                                        </Text>
                                    </View>
                                    <View style={styles.containCount}>
                                        <View style={styles.containerTitleCount}>
                                            <Image source={ImageLoader('icon_yasin')} style={{ width: 7, height: 10, marginRight: 5 }} />
                                            <Text style={styles.titleCount}>Yasin</Text>
                                        </View>
                                        <Text style={styles.valueCount}>
                                            4
                                        </Text>
                                        <Text style={styles.dateCount}>
                                            Terakhir ikut 29/10/2020
                                        </Text>
                                    </View>
                                    <View style={[styles.containCount, { borderRightWidth: 0 }]}>
                                        <View style={styles.containerTitleCount}>
                                            <Image source={ImageLoader('icon_quran')} style={{ width: 9, height: 8, marginRight: 5 }} />
                                            <Text style={[styles.titleCount, { fontSize: 10.5 }]}>Khataman Al Qur’an</Text>
                                        </View>
                                        <Text style={styles.valueCount}>
                                            4
                                        </Text>
                                        <Text style={styles.dateCount}>
                                            Terakhir ikut 29/10/2020
                                        </Text>
                                    </View>
                                </View>
                            </Card>
                        </View>

                        <View style={{ paddingTop: 25, backgroundColor: 'white', width, alignItems: 'center', height: height * 1.5 }}>
                            <View style={{ width: width * 0.9, }}>
                                <Text style={{ fontSize: 14, fontFamily: 'Nunito-Bold', color: '#2B2B2B' }}>
                                    Kategori
                                </Text>
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 13 }}>
                                    <Image source={ImageLoader('tahlil_categori')} style={{ width: 96, height: 103 }} />
                                    <Image source={ImageLoader('yasin_categori')} style={{ width: 96, height: 103 }} />
                                    <TouchableOpacity onPress={() => props.navigation.navigate('Familly')}>
                                        <Image source={ImageLoader('quran_categori')} style={{ width: 96, height: 103 }} />
                                    </TouchableOpacity>

                                </View>
                            </View>

                            <View style={{ width: width * 0.9, flexDirection: "row", justifyContent: "space-between", marginTop: 20 }}>
                                <Text style={{ fontSize: 14, fontFamily: 'Nunito-Bold', color: '#2B2B2B' }}>
                                    Acara
                                </Text>
                                <Text style={{ fontSize: 12, fontFamily: 'Nunito-SemiBold', color: '#939393' }}>
                                    Lihat Semua
                                </Text>
                            </View>

                            <View style={{ height: 240 }}>
                                <ScrollView horizontal={true}>
                                    <View style={{ flexDirection: 'row', marginTop: 13, paddingLeft: width * 0.05, }}>

                                        <TouchableOpacity onPress={() => props.navigation.navigate('DetailEvent')}>
                                            <Card style={{ width: 193, height: 220, borderRadius: 10, marginRight: 10 }}>
                                                <View style={{ width: 193, height: 220, borderRadius: 10, justifyContent: 'space-between' }}>
                                                    <Image source={{ uri: 'https://tni-au.mil.id/konten/unggahan/2019/04/Sambut_Bulan_Ramadhan__1_.jpg' }} style={{ width: 193, height: 113, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} />
                                                    <View style={{ width: '100%', height: 17, padding: 10, flexDirection: 'row', justifyContent: "space-between" }}>
                                                        <View style={{ flexDirection: 'row' }}>
                                                            <Image source={ImageLoader('icon_like')} style={{ width: 15, height: 14, marginRight: 5 }} />
                                                            <Image source={ImageLoader('icon_share')} style={{ width: 16, height: 13, marginRight: 10 }} />
                                                        </View>
                                                        <Image source={ImageLoader('icon_bookmark')} style={{ width: 13, height: 17 }} />
                                                    </View>
                                                    <View style={{ padding: 10, paddingBottom: 5 }}>
                                                        <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 10, color: "#313131", fontFamily: 'Nunito-Bold' }}>Tahlilan Rutin</Text>
                                                        <View style={{ flexDirection: "row", marginTop: 5, justifyContent: 'space-between' }}>
                                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                                <Image source={ImageLoader('icon_calendar')} style={{ width: 10, height: 10, marginRight: 3 }} />
                                                                <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>18 November 2020</Text>
                                                            </View>
                                                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                                <Image source={ImageLoader('icon_clock')} style={{ width: 10, height: 10, marginRight: 3 }} />
                                                                <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>10:20 WIB</Text>
                                                            </View>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                                            <Image source={ImageLoader('icon_location')} style={{ width: 7.4, height: 10, marginRight: 3 }} />
                                                            <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>Pondok Pesantren Darul Hikmah Semarang</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ width: '100%', height: 26, backgroundColor: "#0D7367", borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent: "center", alignItems: 'center' }}>
                                                        <Text style={{ fontFamily: "Nunito-Bold", color: "white", fontSize: 9 }}>Pesan Acara</Text>
                                                    </View>

                                                </View>
                                            </Card>
                                        </TouchableOpacity>
                                        <Card style={{ width: 193, height: 220, borderRadius: 10, marginRight: 10 }}>
                                            <View style={{ width: 193, height: 220, borderRadius: 10, justifyContent: 'space-between' }}>
                                                <Image source={{ uri: 'https://tni-au.mil.id/konten/unggahan/2019/04/Sambut_Bulan_Ramadhan__1_.jpg' }} style={{ width: 193, height: 113, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} />
                                                <View style={{ width: '100%', height: 17, padding: 10, flexDirection: 'row', justifyContent: "space-between" }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Image source={ImageLoader('icon_like')} style={{ width: 15, height: 14, marginRight: 5 }} />
                                                        <Image source={ImageLoader('icon_share')} style={{ width: 16, height: 13, marginRight: 10 }} />
                                                    </View>
                                                    <Image source={ImageLoader('icon_bookmark')} style={{ width: 13, height: 17 }} />
                                                </View>
                                                <View style={{ padding: 10, paddingBottom: 5 }}>
                                                    <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 10, color: "#313131", fontFamily: 'Nunito-Bold' }}>Tahlilan Rutin</Text>
                                                    <View style={{ flexDirection: "row", marginTop: 5, justifyContent: 'space-between' }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image source={ImageLoader('icon_calendar')} style={{ width: 10, height: 10, marginRight: 3 }} />
                                                            <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>18 November 2020</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image source={ImageLoader('icon_clock')} style={{ width: 10, height: 10, marginRight: 3 }} />
                                                            <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>10:20 WIB</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                                        <Image source={ImageLoader('icon_location')} style={{ width: 7.4, height: 10, marginRight: 3 }} />
                                                        <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>Pondok Pesantren Darul Hikmah Semarang</Text>
                                                    </View>
                                                </View>
                                                <View style={{ width: '100%', height: 26, backgroundColor: "#0D7367", borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent: "center", alignItems: 'center' }}>
                                                    <Text style={{ fontFamily: "Nunito-Bold", color: "white", fontSize: 9 }}>Pesan Acara</Text>
                                                </View>

                                            </View>
                                        </Card>
                                        <Card style={{ width: 193, height: 220, borderRadius: 10, marginRight: 10 }}>
                                            <View style={{ width: 193, height: 220, borderRadius: 10, justifyContent: 'space-between' }}>
                                                <Image source={{ uri: 'https://tni-au.mil.id/konten/unggahan/2019/04/Sambut_Bulan_Ramadhan__1_.jpg' }} style={{ width: 193, height: 113, borderTopLeftRadius: 10, borderTopRightRadius: 10 }} />
                                                <View style={{ width: '100%', height: 17, padding: 10, flexDirection: 'row', justifyContent: "space-between" }}>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <Image source={ImageLoader('icon_like')} style={{ width: 15, height: 14, marginRight: 5 }} />
                                                        <Image source={ImageLoader('icon_share')} style={{ width: 16, height: 13, marginRight: 10 }} />
                                                    </View>
                                                    <Image source={ImageLoader('icon_bookmark')} style={{ width: 13, height: 17 }} />
                                                </View>
                                                <View style={{ padding: 10, paddingBottom: 5 }}>
                                                    <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 10, color: "#313131", fontFamily: 'Nunito-Bold' }}>Tahlilan Rutin</Text>
                                                    <View style={{ flexDirection: "row", marginTop: 5, justifyContent: 'space-between' }}>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image source={ImageLoader('icon_calendar')} style={{ width: 10, height: 10, marginRight: 3 }} />
                                                            <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>18 November 2020</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                                            <Image source={ImageLoader('icon_clock')} style={{ width: 10, height: 10, marginRight: 3 }} />
                                                            <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>10:20 WIB</Text>
                                                        </View>
                                                    </View>
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 5 }}>
                                                        <Image source={ImageLoader('icon_location')} style={{ width: 7.4, height: 10, marginRight: 3 }} />
                                                        <Text style={{ fontSize: 7, color: "#757575", fontFamily: 'Nunito-Bold' }}>Pondok Pesantren Darul Hikmah Semarang</Text>
                                                    </View>
                                                </View>
                                                <View style={{ width: '100%', height: 26, backgroundColor: "#0D7367", borderBottomLeftRadius: 10, borderBottomRightRadius: 10, justifyContent: "center", alignItems: 'center' }}>
                                                    <Text style={{ fontFamily: "Nunito-Bold", color: "white", fontSize: 9 }}>Pesan Acara</Text>
                                                </View>

                                            </View>
                                        </Card>


                                    </View>
                                </ScrollView>
                            </View>
                            <View style={{ width: width * 0.9, marginTop: 20 }}>
                                <View style={{ width: '100%', flexDirection: "row", justifyContent: "space-between", marginBottom: 10 }}>
                                    <Text style={{ fontSize: 14, fontFamily: 'Nunito-Bold', color: '#2B2B2B' }}>
                                        Transaksi Terakhir
                                </Text>
                                    <Text style={{ fontSize: 12, fontFamily: 'Nunito-SemiBold', color: '#939393' }}>
                                        Lihat Semua
                                </Text>
                                </View>

                                <View style={{ width: '100%', height: 68, backgroundColor: '#F5F6F8', marginTop: 10, borderRadius: 10, padding: 15, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>

                                    <Image source={ImageLoader('tahlil_categori')} style={{ width: 36, height: 36 }} />
                                    <View style={{ width: '60%' }}>
                                        <Text style={{ color: '#0D7367', fontFamily: "Nunito-Bold", fontSize: 14, }}>
                                            Tahlil
                                        </Text>
                                        <Text style={{ color: '#0D7367', fontFamily: "Nunito-Bold", fontSize: 7, }}>
                                            12 November 2020
                                        </Text>
                                        <Text style={{ color: '#939393', fontFamily: "Nunito-Bold", fontSize: 7, }}>
                                            Pondok Pesantren Darul Hikmah Semarang
                                        </Text>
                                    </View>
                                    <View style={{ width: 57, height: 21, backgroundColor: '#0D7367', borderRadius: 10.5, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 10, fontFamily: 'Nunito-Bold', color: "white" }}>Detail</Text>
                                    </View>
                                </View>
                                <View style={{ width: '100%', height: 68, backgroundColor: '#F5F6F8', marginTop: 10, borderRadius: 10, padding: 15, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>

                                    <Image source={ImageLoader('yasin_categori')} style={{ width: 36, height: 36 }} />
                                    <View style={{ width: '60%' }}>
                                        <Text style={{ color: '#0D7367', fontFamily: "Nunito-Bold", fontSize: 14, }}>
                                            Yasin
                                         </Text>
                                        <Text style={{ color: '#0D7367', fontFamily: "Nunito-Bold", fontSize: 7, }}>
                                            12 November 2020
                                        </Text>
                                        <Text style={{ color: '#939393', fontFamily: "Nunito-Bold", fontSize: 7, }}>
                                            Pondok Pesantren Darul Hikmah Semarang
                                        </Text>
                                    </View>
                                    <View style={{ width: 57, height: 21, backgroundColor: '#0D7367', borderRadius: 10.5, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 10, fontFamily: 'Nunito-Bold', color: "white" }}>Detail</Text>
                                    </View>
                                </View>
                                <View style={{ width: '100%', height: 68, backgroundColor: '#F5F6F8', marginTop: 10, borderRadius: 10, padding: 15, flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>

                                    <Image source={ImageLoader('quran_categori')} style={{ width: 36, height: 36 }} />
                                    <View style={{ width: '60%' }}>
                                        <Text style={{ color: '#0D7367', fontFamily: "Nunito-Bold", fontSize: 14, }}>
                                            Khataman Al Qur’an
                                         </Text>
                                        <Text style={{ color: '#0D7367', fontFamily: "Nunito-Bold", fontSize: 7, }}>
                                            12 November 2020
                                        </Text>
                                        <Text style={{ color: '#939393', fontFamily: "Nunito-Bold", fontSize: 7, }}>
                                            Pondok Pesantren Darul Hikmah Semarang
                                        </Text>
                                    </View>
                                    <View style={{ width: 57, height: 21, backgroundColor: '#0D7367', borderRadius: 10.5, justifyContent: 'center', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 10, fontFamily: 'Nunito-Bold', color: "white" }}>Detail</Text>
                                    </View>
                                </View>
                            </View>


                        </View>
                    </Animatable.View>

                </View>

            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    MainContainer: { width, height: '100%', backgroundColor: 'white' },
    containerCount: { width: width * 0.9, height: 82, borderRadius: 5, flexDirection: 'row', justifyContent: 'space-between', padding: 10 },
    containCount: { width: '33%', justifyContent: "space-between", borderRightWidth: 1, borderRightColor: '#CECECE', paddingHorizontal: 5, },
    containerTitleCount: { height: 20, flexDirection: 'row', alignItems: "center" },
    titleCount: { fontFamily: 'Nunito-Bold', color: '#0D7367', fontSize: 11 },
    valueCount: { fontFamily: 'Nunito-ExtraBold', fontSize: 20, color: '#0D7367' },
    dateCount: { fontSize: 7, color: '#645A5A', }
})