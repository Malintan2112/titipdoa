import React, { useEffect, useState, useRef } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions,
    ActivityIndicator,
    Animated
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';
import HeaderItems from '../../components/Header';
import { Content, Input } from 'native-base';

import ImageLoader from "../../helpers/ImagesLoader";
import { Card } from 'native-base';
const { width, height } = Dimensions.get('window');
import RBSheet from "react-native-raw-bottom-sheet";


export default Familly = ({ navigation, route }) => {
    const [scrollY] = useState(new Animated.Value(0))
    const [form, setForm] = useState({});

    const opacity = scrollY.interpolate({
        inputRange: [0, 30],
        outputRange: [1, 0],
        extrapolate: 'clamp'

    })

    const refRBSheet = useRef();

    const [listName, setListName] = useState([{ name: 'Nanang Kurnia Wuhub', date_die: '17 Agustus 1945', weton: 'Jumat Kliwon' }])
    const rbSheetContent = () => (
        <RBSheet
            ref={refRBSheet}
            onClose={(ref) => {

            }}

            openDuration={100}
            animationType={'slide'}
            dragFromTopOnly={true}
            customStyles={{
                container: {
                    paddingHorizontal: width * 0.08,
                    paddingVertical: width * 0.08,
                    borderTopRightRadius: 30,
                    borderTopLeftRadius: 30,
                    height: 380
                }
            }}
        >

            <View>
                <Text style={{ fontFamily: 'Nunito-Bold' }}>Tambah Daftar Almarhum</Text>
            </View>
            <View style={{ marginTop: 20, marginBottom: 10 }}>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    Nama
                </Text>
                <View style={{ width: '100%', height: 40, borderBottomColor: '#747474', borderBottomWidth: 1 }}>
                    <Input
                        selectionColor="#2C2E34"
                        onChangeText={(text) => setForm({ ...form, name: text })}
                        style={{ color: 'black', width: '70%', fontSize: 16 }} numberOfLines={1} ellipsizeMode="tail"
                        value={form.name}
                        placeholderTextColor="#747474"
                        placeholder="Masukkan Nama"
                        keyboardType='default'
                    />
                </View>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    Tanggal Meninggal
                </Text>
                <View style={{ width: '100%', height: 40, borderBottomColor: '#747474', borderBottomWidth: 1 }}>
                    <Input
                        selectionColor="#2C2E34"
                        onChangeText={(text) => setForm({ ...form, date_die: text })}
                        style={{ color: 'black', width: '70%', fontSize: 16 }} numberOfLines={1} ellipsizeMode="tail"
                        value={form.date_die}
                        placeholderTextColor="#747474"
                        placeholder="dd/mm/yyyy"
                        keyboardType='default'
                    />
                </View>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    Hari Weton Meninggal <Text style={{ fontSize: 10 }}>( Optional )</Text>
                </Text>
                <View style={{ width: '100%', height: 40, borderBottomColor: '#747474', borderBottomWidth: 1 }}>
                    <Input
                        selectionColor="#2C2E34"
                        onChangeText={(text) => setForm({ ...form, weton: text })}
                        style={{ color: 'black', width: '70%', fontSize: 16 }} numberOfLines={1} ellipsizeMode="tail"
                        value={form.weton}
                        placeholderTextColor="#747474"
                        placeholder="Masukan Weton "
                        keyboardType='default'
                    />
                </View>

            </View>
            <TouchableOpacity onPress={() => {
                const reForm = [...listName];
                reForm.push({ name: form.name, date_die: form.date_die, weton: form.weton })
                setListName(reForm)
                setForm({})
                refRBSheet.current.close()
            }} style={{ width: width * 0.8, height: 46, backgroundColor: '#0D7367', borderRadius: 23, marginTop: 20, justifyContent: "center", alignItems: 'center', alignSelf: 'center' }}>
                <Text style={{ color: 'white' }}>Simpan Nama</Text>
            </TouchableOpacity>
        </RBSheet>
    )
    return (
        <>
            {rbSheetContent()}
            <HeaderItems back={true} title='Keluargaku' navigation={navigation} />
            <ScrollView onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], { useNativeDriver: false })}>
                <View style={styles.MainContainer}>
                    <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 12 }}>Daftar Nama Almarhum</Text>
                    {listName.map((data, index) => (
                        <View key={index} style={{ width: '100%', marginTop: 10, borderBottomColor: '#EFEFEF', borderBottomWidth: 1.2, minHeight: 70, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                            <View style={{ width: '80%' }}>
                                <Text style={{ fontSize: 14, fontFamily: 'Nunito-SemiBold' }}>{data.name}</Text>
                                <Text style={{ fontSize: 11, color: '#747474' }}>{data.weton}, {data.date_die}</Text>
                            </View>
                            <View>
                                <Image source={ImageLoader('edit')} style={{ width: 20, height: 20 }} />
                            </View>
                        </View>
                    ))}
                    <TouchableOpacity onPress={() => refRBSheet.current.open()} style={{ width: width * 0.8, height: 46, backgroundColor: '#0D7367', borderRadius: 23, marginTop: 20, justifyContent: "center", alignItems: 'center', alignSelf: 'center' }}>
                        <Text style={{ color: 'white' }}>Tambah Nama</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </>
    )
}
const styles = StyleSheet.create({
    MainContainer: { width, minHeight: height, backgroundColor: 'white', paddingHorizontal: width * 0.07, paddingVertical: width * 0.08 },
});