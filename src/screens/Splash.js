import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import ImageLoader from "../helpers/ImagesLoader";
const { width, height } = Dimensions.get('window');


export default Splash = ({ navigation }) => {
    return (
        <View style={{ flex: 1, backgroundColor: 'white', justifyContent: "center", alignItems: 'center', position: 'relative' }}>
            <Image source={ImageLoader('logo')} style={{ width: 90, height: 90 }} />

            <View style={{ position: 'absolute', bottom: 20 }}>
                <Text style={{ fontSize: 10 }}> Waladi Apps V.0.0.1  ©2020</Text>
            </View>
        </View>
    )
}