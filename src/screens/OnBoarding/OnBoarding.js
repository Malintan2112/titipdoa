import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions
} from 'react-native';
import ImageLoader from "../../helpers/ImagesLoader";
import AppIntroSlider from '../../components/AppIntroSlider';

const { width, height } = Dimensions.get('window');


export default OnBoarding = ({ navigation }) => {

    const slides = [
        {
            key: 1,
            title: 'Selamat Datang',
            text: 'Waladi adalah sebuah aplikasi layanan yang membantu anda untuk mendoakan orang anda sayangi yang sudah meninggalkan kita di dunia',
            image: ImageLoader('ob1'),
            backgroundColor: '#59b2ab',
        },
        {
            key: 2,
            title: 'Ayo Berdo’a',
            text: 'Mari mendoakan dan bersedekah untuk \n orang yang kita sayangi yang telah mendahului kita',
            image: ImageLoader('ob2'),
            backgroundColor: '#febe29',
        },

    ];
    const _renderItem = ({ item }) => {
        return (
            <View style={{ width: '100%', height: '100%', justifyContent: 'center', alignItems: "center" }}>
                <View style={{ height: 250 }}>
                    <Image source={item.image} style={{ width: 310, height: 224.52 }} />
                </View>
                <View style={{ width: '75%', alignItems: 'center', height: 200 }}>
                    <Text style={{ fontFamily: 'Nunito-ExtraBold', fontSize: 26, color: '#0D7367', textAlign: 'center', marginBottom: 20 }}>{item.title}</Text>
                    <Text style={{ textAlign: 'center', fontSize: 13, color: '#686868', marginTop: 10, textAlignVertical: 'center', lineHeight: 22, }} >{item.text}</Text>
                </View>

            </View>
        );
    }
    const renderNext = () => {
        return (
            <View style={{ width, alignItems: 'center' }}>
                <View style={{ width: 180, height: 54, backgroundColor: '#0D7367', borderRadius: 27, justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ color: 'white', fontFamily: 'Nunito-Bold', }}>Selanjutnya</Text>
                </View>
            </View>
        )
    }
    const renderDone = () => {
        return (
            <View style={{ width, alignItems: 'center' }}>
                <View style={{ width: 180, height: 54, backgroundColor: '#0D7367', borderRadius: 27, justifyContent: 'center', alignItems: 'center', }}>
                    <Text style={{ color: 'white', fontFamily: 'Nunito-Bold', }}>Mulai</Text>
                </View>
            </View>
        )
    }
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <AppIntroSlider renderItem={_renderItem} data={slides} activeDotStyle={{ backgroundColor: '#0D7367', width: 12, height: 12, borderRadius: 6 }} dotStyle={{ backgroundColor: 'rgba(0,101,161,0.5)', width: 8, height: 8, borderRadius: 4 }} renderNextButton={renderNext} renderDoneButton={renderDone}
                onDone={() => {
                    console.log('Done')
                    navigation.navigate('Login')
                }} />
        </View>
    )
}