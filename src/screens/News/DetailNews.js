import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    Image
} from 'react-native';
import HeaderItems from '../../components/Header';
const { width, height } = Dimensions.get('window');
import ImageLoader from "../../helpers/ImagesLoader";

import { Card } from 'native-base';

export default DetailNews = ({ navigation }) => {
    return (
        <>
            <HeaderItems back={true} title='Detail News' navigation={navigation} />
            <View style={{ marginTop: 10 }} />
            <ScrollView>
                <Card style={{ width: width * 0.85, alignSelf: 'center', borderRadius: 10 }}>
                    <View style={{ width: '100%', borderRadius: 10 }}>
                        <View>
                            <Image resizeMode='cover' source={{ uri: 'https://assets-a1.kompasiana.com/items/album/2018/10/06/img-0493-5bb856e943322f339921f097.jpg' }} style={{ width: '100%', height: 150, borderTopRightRadius: 10, borderTopLeftRadius: 10, alignSelf: "center" }} />

                        </View>
                        <View style={{ padding: 10, paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                <Image source={ImageLoader('post_by')} style={{ width: 10, height: 10 }} />
                                <Text style={styles.note}>Post by : Najib Ibrahim</Text>
                                <Image source={ImageLoader('date_time_news')} style={{ width: 11, height: 11, marginLeft: 10 }} />

                                <Text style={styles.note}>22:30 | 22-09-2020</Text>

                            </View>
                            <View style={{ marginTop: 7 }}>
                                <Text ellipsizeMode='tail' style={{ fontFamily: 'Montserrat-Bold', fontSize: 15, color: '#0068A1' }}>
                                    Perbaikan Mesin Pompan di Depan Rumah Pak Panut
                                </Text>
                            </View>
                            <View style={{ marginTop: 7, }}>
                                <Text style={{ textAlign: 'justify', fontSize: 12, color: '#646464', flex: 1, flexWrap: 'wrap' }}>
                                    Selama bertahun-tahun, Olympia Beer (Tumwater, Washington) telah memroduksi bir dengan air yang diperoleh dari sumur artesis. Promosi perusahaan mengakatan banyanye periklanan.ak air artesis dipakai dalam proses pembuatan bir. Tetapi, iklannya tak pernah menjelaskan apa itu air artesis, dan mengklaim bahwa air tersebut dikontrol oleh penduduk "Artesian" yang dianggap sebagai mitos.[2] Setelah tempat pembuatan bir ini diambil alih oleh perusahaan yang lebih besar, penggunaan air artesis dihentikan, dan begitu juga kamp {"\n"} Promosi perusahaan mengakatan banyanye periklanan.ak air artesis dipakai dalam proses pembuatan bir. Tetapi, iklannya tak pernah menjelaskan apa itu air artesis, dan mengklaim bahwa air tersebut dikontrol oleh penduduk "Artesian" yang dianggap sebagai mitos.[2] Setelah tempat pembuatan bir ini diambil alih oleh perusahaan yang lebih besar, penggunaan air artesis dihentikan, dan begitu juga kamp
                                </Text>
                            </View>
                        </View>
                    </View>
                </Card>

            </ScrollView>

        </>
    )
}
const styles = StyleSheet.create({
    note: { fontFamily: 'Montserrat-Medium', fontSize: 8, color: '#646464', marginHorizontal: 7 }
})