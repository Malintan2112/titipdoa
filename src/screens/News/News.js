import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    Image
} from 'react-native';
import HeaderItems from '../../components/Header';
const { width, height } = Dimensions.get('window');
import ImageLoader from "../../helpers/ImagesLoader";

import { Card } from 'native-base';

export default News = ({ navigation }) => {
    return (
        <>
            <HeaderItems back={false} title='News' navigation={navigation} />
            <View style={{ marginTop: 10 }} />
            <ScrollView>

                <Card style={{ width: width * 0.85, height: 290, alignSelf: 'center', borderRadius: 10 }}>
                    <TouchableOpacity style={{ width: '100%', height: 290, borderRadius: 10 }}>
                        <View>
                            <Image resizeMode='cover' source={{ uri: 'https://assets-a1.kompasiana.com/items/album/2018/10/06/img-0493-5bb856e943322f339921f097.jpg' }} style={{ width: '100%', height: 150, borderTopRightRadius: 10, borderTopLeftRadius: 10, alignSelf: "center" }} />
                        </View>
                        <View style={{ padding: 10, paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                <Image source={ImageLoader('post_by')} style={{ width: 10, height: 10 }} />
                                <Text style={styles.note}>Post by : Najib Ibrahim</Text>
                                <Image source={ImageLoader('date_time_news')} style={{ width: 11, height: 11, marginLeft: 10 }} />
                                <Text style={styles.note}>22:30 | 22-09-2020</Text>

                            </View>
                            <View style={{ marginTop: 7 }}>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Montserrat-Bold', fontSize: 14, color: '#0068A1' }}>
                                    Perbaikan Mesin Pompan di Depan Rumah Pak Panut
                                </Text>
                            </View>
                            <View style={{ marginTop: 5 }}>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontSize: 10, color: '#646464' }}>
                                    Sebuah akuifer artesis adalah sebuah akuifer terbatas berisi air tanah yang akan mengalir ke atas melalui sebuah sumur yang disebut sumur artesis tanpa perlu dipompa. Air dapat mencapai permukaan tanah apabila tekanan alaminya cukup tinggi, dalam hal ini sumur itu disebut sumur artesis mengalir.
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('DetailNews')}
                                style={{ width: 90, height: 22, backgroundColor: "#0068A1", alignSelf: 'flex-end', borderRadius: 11, marginTop: 7, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 12, fontFamily: "Montserrat-SemiBold" }}>Detail</Text>
                            </TouchableOpacity>
                        </View>

                    </TouchableOpacity>
                </Card>
                <Card style={{ width: width * 0.85, height: 290, alignSelf: 'center', borderRadius: 10 }}>
                    <TouchableOpacity style={{ width: '100%', height: 290, borderRadius: 10 }}>
                        <View>
                            <Image resizeMode='cover' source={{ uri: 'https://assets-a1.kompasiana.com/items/album/2018/10/06/img-0493-5bb856e943322f339921f097.jpg' }} style={{ width: '100%', height: 150, borderTopRightRadius: 10, borderTopLeftRadius: 10, alignSelf: "center" }} />
                        </View>
                        <View style={{ padding: 10, paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                <Image source={ImageLoader('post_by')} style={{ width: 10, height: 10 }} />
                                <Text style={styles.note}>Post by : Najib Ibrahim</Text>
                                <Image source={ImageLoader('date_time_news')} style={{ width: 11, height: 11, marginLeft: 10 }} />
                                <Text style={styles.note}>22:30 | 22-09-2020</Text>

                            </View>
                            <View style={{ marginTop: 7 }}>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Montserrat-Bold', fontSize: 14, color: '#0068A1' }}>
                                    Perbaikan Mesin Pompan di Depan Rumah Pak Panut
                                </Text>
                            </View>
                            <View style={{ marginTop: 5 }}>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontSize: 10, color: '#646464' }}>
                                    Sebuah akuifer artesis adalah sebuah akuifer terbatas berisi air tanah yang akan mengalir ke atas melalui sebuah sumur yang disebut sumur artesis tanpa perlu dipompa. Air dapat mencapai permukaan tanah apabila tekanan alaminya cukup tinggi, dalam hal ini sumur itu disebut sumur artesis mengalir.
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('DetailNews')}
                                style={{ width: 90, height: 22, backgroundColor: "#0068A1", alignSelf: 'flex-end', borderRadius: 11, marginTop: 7, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 12, fontFamily: "Montserrat-SemiBold" }}>Detail</Text>
                            </TouchableOpacity>
                        </View>

                    </TouchableOpacity>
                </Card>
                <Card style={{ width: width * 0.85, height: 290, alignSelf: 'center', borderRadius: 10 }}>
                    <TouchableOpacity style={{ width: '100%', height: 290, borderRadius: 10 }}>
                        <View>
                            <Image resizeMode='cover' source={{ uri: 'https://assets-a1.kompasiana.com/items/album/2018/10/06/img-0493-5bb856e943322f339921f097.jpg' }} style={{ width: '100%', height: 150, borderTopRightRadius: 10, borderTopLeftRadius: 10, alignSelf: "center" }} />
                        </View>
                        <View style={{ padding: 10, paddingHorizontal: 15 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                                <Image source={ImageLoader('post_by')} style={{ width: 10, height: 10 }} />
                                <Text style={styles.note}>Post by : Najib Ibrahim</Text>
                                <Image source={ImageLoader('date_time_news')} style={{ width: 11, height: 11, marginLeft: 10 }} />
                                <Text style={styles.note}>22:30 | 22-09-2020</Text>

                            </View>
                            <View style={{ marginTop: 7 }}>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Montserrat-Bold', fontSize: 14, color: '#0068A1' }}>
                                    Perbaikan Mesin Pompan di Depan Rumah Pak Panut
                                </Text>
                            </View>
                            <View style={{ marginTop: 5 }}>
                                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontSize: 10, color: '#646464' }}>
                                    Sebuah akuifer artesis adalah sebuah akuifer terbatas berisi air tanah yang akan mengalir ke atas melalui sebuah sumur yang disebut sumur artesis tanpa perlu dipompa. Air dapat mencapai permukaan tanah apabila tekanan alaminya cukup tinggi, dalam hal ini sumur itu disebut sumur artesis mengalir.
                                </Text>
                            </View>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('DetailNews')}
                                style={{ width: 90, height: 22, backgroundColor: "#0068A1", alignSelf: 'flex-end', borderRadius: 11, marginTop: 7, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ color: 'white', fontSize: 12, fontFamily: "Montserrat-SemiBold" }}>Detail</Text>
                            </TouchableOpacity>
                        </View>

                    </TouchableOpacity>
                </Card>


            </ScrollView>

        </>
    )
}
const styles = StyleSheet.create({
    note: { fontFamily: 'Montserrat-Medium', fontSize: 8, color: '#646464', marginHorizontal: 7 }
})