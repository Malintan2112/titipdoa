import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions,
    ActivityIndicator,
    Animated
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';
import HeaderItems from '../../components/Header';

import ImageLoader from "../../helpers/ImagesLoader";
import { Card } from 'native-base';
const { width, height } = Dimensions.get('window');

export default Profile = ({ navigation, route }) => {
    const [scrollY] = useState(new Animated.Value(0))
    const opacity = scrollY.interpolate({
        inputRange: [0, 30],
        outputRange: [1, 0],
        extrapolate: 'clamp'

    })

    return (
        <>
            <HeaderItems back={false} title='Profile' navigation={navigation} />
            <ScrollView onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], { useNativeDriver: false })}>
                <View style={styles.MainContainer}>
                    <Animatable.View
                        animation="fadeInDown"
                        duration={1000}
                    >
                        <Animated.View style={{
                            width, height: 160, backgroundColor: '#0D7367', justifyContent: 'space-around', alignItems: 'center', opacity: opacity
                        }}>
                            <View style={{ backgroundColor: 'white', borderRadius: 25, height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 16, fontFamily: 'Nunito-Bold', color: '#0D7367' }}>AT</Text>
                            </View>
                            <View style={{ alignItems: 'center', marginBottom: 20 }}>
                                <Text style={{ fontFamily: 'Nunito-Bold', color: 'white', fontSize: 18 }}>
                                    Cristiano Ronaldo
                            </Text>
                                <Text style={{ color: 'white', fontSize: 13 }}>
                                    cristiano@cr7.com
                            </Text>
                            </View>
                        </Animated.View>
                    </Animatable.View>
                    <Animatable.View
                        animation="slideInUp"
                        duration={1000}
                    >
                        <View style={{ width, height: 53, backgroundColor: '#F9F9F9', padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Nunito-ExtraBold', fontSize: 11 }}>DATA PRIBADI</Text>
                            <Text style={{ fontFamily: 'Nunito-ExtraBold', fontSize: 11, color: '#E32B2D' }}>UBAH</Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Nama
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                Crsitiano Ronaldo
                        </Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Jenis Kelamin
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                Laki - Laki
                        </Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Tanggal Lahir
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                21/12/1996
                        </Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                No HandPhone
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                08980087646
                        </Text>
                        </View>
                        <View style={{ width, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Alamat
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                Jl. Nama Jalan No. 00, Pedurungan Lor, Pedurungan
                        </Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Kota
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                Semarang
                        </Text>
                        </View>
                        <View style={{ width, height: 53, backgroundColor: '#F9F9F9', padding: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                            <Text style={{ fontFamily: 'Nunito-ExtraBold', fontSize: 11 }}>AKUN</Text>
                            <Text style={{ fontFamily: 'Nunito-ExtraBold', fontSize: 11, color: '#E32B2D' }}>UBAH</Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Email
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                malintanDev@gmail.com
                        </Text>
                        </View>
                        <View style={{ width, height: 53, borderBottomColor: '#EFEFEF', padding: 20, paddingVertical: 10, borderBottomWidth: 1, justifyContent: "center" }}>
                            <Text style={{ fontSize: 11 }}>
                                Password
                        </Text>
                            <Text style={{ color: '#747474', fontSize: 13 }}>
                                ******
                        </Text>
                        </View>
                    </Animatable.View>
                </View>
            </ScrollView>
        </>
    )
}
const styles = StyleSheet.create({
    MainContainer: { width, height: '100%', backgroundColor: 'white' },
});