import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';

import ImageLoader from "../../helpers/ImagesLoader";
const { width, height } = Dimensions.get('window');

export default Register = ({ navigation }) => {
    return (
        <View style={styles.MainContainer}>
            <StatusBar translucent backgroundColor="transparent" barStyle="light-content" />

            <ScrollView>
                <View style={{ position: 'relative', width: '100%', height: height * 0.83 }}>
                    <Animatable.Image
                        animation="slideInDown"
                        duration={1000}

                        source={ImageLoader('login_background')} style={{ width: '100%', height: height * 0.81 }} />
                    <View style={styles.login}>
                        <View style={styles.loginInContainer}>
                            <Text style={styles.loginTitle}>
                                Register
                            </Text>
                            <View style={{ width: 90, height: 3, backgroundColor: 'white', borderRadius: 3, marginTop: 10, marginBottom: 30 }} />
                            <View style={styles.containerTextInput}>
                                <View style={{ width: '15%', paddingTop: 25 }}>
                                    <Image source={ImageLoader('user_form')} style={{ width: 15, height: 15 }} />
                                </View>
                                <View style={styles.containerTitle}>
                                    <Text style={styles.textInputTitle}>
                                        Nama
                                    </Text>
                                    <TextInput placeholderTextColor="white" placeholder='example : Jhon' style={{ color: 'white' }} />

                                </View>

                            </View>
                            <View style={styles.containerTextInput}>
                                <View style={{ width: '15%', paddingTop: 25 }}>
                                    <Image source={ImageLoader('phone')} style={{ width: 11, height: 20 }} />
                                </View>
                                <View style={styles.containerTitle}>
                                    <Text style={styles.textInputTitle}>
                                        Handphone
                                    </Text>
                                    <TextInput placeholderTextColor="white" placeholder='0893232**' style={{ color: 'white' }} />

                                </View>

                            </View>
                            <View style={styles.containerTextInput}>
                                <View style={{ width: '15%', paddingTop: 25 }}>
                                    <Image source={ImageLoader('mail')} style={{ width: 19, height: 15 }} />
                                </View>
                                <View style={styles.containerTitle}>
                                    <Text style={styles.textInputTitle}>
                                        Email
                                    </Text>
                                    <TextInput placeholderTextColor="white" placeholder='example@email.com' style={{ color: 'white' }} />

                                </View>

                            </View>
                            <View style={styles.containerTextInput}>
                                <View style={{ width: '15%', paddingTop: 25 }}>
                                    <Image source={ImageLoader('key')} style={{ width: 17, height: 17 }} />
                                </View>
                                <View style={styles.containerTitle}>
                                    <Text style={styles.textInputTitle}>
                                        Password
                                    </Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <TextInput placeholderTextColor="white" style={{ width: '90%', color: 'white' }} placeholder='******' secureTextEntry={true} />
                                        <Image source={ImageLoader('eye')} style={{ width: 18, height: 16 }} />
                                    </View>
                                </View>
                            </View>


                        </View>
                        <View style={{ width: width * 0.4, height: 46, backgroundColor: 'black', borderRadius: 23, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ color: 'white', fontFamily: 'Nunito-Bold', fontSize: 16 }}>
                                Daftar
                        </Text>
                        </View>
                    </View>
                </View>
                <Animatable.View
                    animation="slideInUp"
                    duration={1000}
                    style={{ width: '100%', paddingHorizontal: width * 0.1, marginTop: 30, alignItems: 'center' }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <View style={{ width: '15%', height: 1, backgroundColor: '#BEBEBE', borderRadius: 1 }} />
                        <Text style={{ marginHorizontal: 10, fontFamily: 'Nunito-SemiBold', fontSize: 12, color: '#959595' }}>Atau Register dengan</Text>
                        <View style={{ width: '15%', height: 1, backgroundColor: '#BEBEBE', borderRadius: 1 }} />
                    </View>

                    <View style={{ width: '100%', marginTop: 30, flexDirection: 'row', justifyContent: 'space-between' }}>
                        <View style={{ width: '45%', backgroundColor: '#0E3D93', height: 42, borderRadius: 21, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={ImageLoader('facebook')} style={{ width: 7.5, height: 15, marginRight: 10 }} />
                            <Text style={{ fontFamily: 'Nunito-SemiBold', color: 'white', fontSize: 14 }}>Facebook</Text>
                        </View>

                        <View style={{ width: '45%', backgroundColor: '#D91407', height: 42, borderRadius: 21, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                            <Image source={ImageLoader('google')} style={{ width: 20, height: 12, marginRight: 10 }} />
                            <Text style={{ fontFamily: 'Nunito-SemiBold', color: 'white', fontSize: 14 }}>Google</Text>
                        </View>

                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 30, marginBottom: 20 }}>
                        <Text style={{ fontFamily: 'Nunito-SemiBold', fontSize: 12, color: '#959595' }}>
                            Sudah memiliki akun ?
                    </Text>

                        <TouchableOpacity
                            onPress={() => navigation.navigate('Login')}
                        >
                            <Text style={{ color: '#0D7367', fontFamily: 'Nunito-Bold', fontSize: 12, }}> Login</Text>
                        </TouchableOpacity>
                    </View>
                </Animatable.View>
            </ScrollView>
        </View>
    )
}
const styles = StyleSheet.create({
    MainContainer: {
        flex: 1,
        backgroundColor: 'white',
    },
    login: { position: "absolute", justifyContent: 'space-between', alignItems: "center", width: '100%', height: height * 0.83 },
    loginInContainer: { width: '100%', height: height * 0.6, paddingHorizontal: width * 0.1, paddingTop: height * 0.1, justifyContent: 'center' },
    loginTitle: { color: 'white', fontFamily: 'Nunito-Bold', fontSize: 32, marginTop: 60 },
    textInputTitle: { fontFamily: 'Nunito-SemiBold', color: 'white', fontSize: 12 },
    containerTitle: { height: 58, width: '85%', borderBottomColor: 'white', borderBottomWidth: 1 },
    containerTextInput: { flexDirection: 'row', marginTop: height * 0.03, alignItems: 'center' }
})