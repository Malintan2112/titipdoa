import React, { useEffect } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Dimensions,
    Image
} from 'react-native';
import HeaderItems from '../../components/Header';
const { width, height } = Dimensions.get('window');
import ImageLoader from "../../helpers/ImagesLoader";

import { Card } from 'native-base';

export default DetailEvent = ({ navigation }) => {
    return (
        <>
            <HeaderItems back={true} title='Yasinan' navigation={navigation} />
            <ScrollView>
                <View style={{ width: '100%', minHeight: height, backgroundColor: 'white' }}>
                    <View style={{ width: '95%', alignSelf: 'center', marginTop: 20, flexDirection: 'row', alignItems: 'center', }}>
                        <View style={{ backgroundColor: 'gray', borderRadius: 25, height: 50, width: 50, justifyContent: 'center', alignItems: 'center' }}>
                            <Text style={{ fontSize: 16, fontFamily: 'Nunito-Bold', color: 'white' }}>AT</Text>
                        </View>
                        <Text style={{ marginLeft: 10, fontSize: 12 }}>
                            Pondok Pesantren Apa Semarang
                        </Text>
                    </View>
                    <Image resizeMode='cover' source={{ uri: 'https://tni-au.mil.id/konten/unggahan/2019/04/Sambut_Bulan_Ramadhan__1_.jpg' }} style={{ width: '100%', height: 200, alignSelf: "center", backgroundColor: 'red', marginTop: 20 }} />
                    <View style={{ width: '100%', height: 17, paddingHorizontal: 20, flexDirection: 'row', justifyContent: "space-between", marginTop: 20 }}>
                        <View style={{ flexDirection: 'row' }}>
                            <Image source={ImageLoader('icon_like')} style={{ width: 18, height: 17, marginRight: 5 }} />
                            <Image source={ImageLoader('icon_share')} style={{ width: 18, height: 15, marginRight: 10 }} />
                        </View>
                        <Image source={ImageLoader('icon_bookmark')} style={{ width: 15, height: 19 }} />
                    </View>
                    <View style={{ width: '100%', alignSelf: 'center', paddingHorizontal: 20, }}>
                        <View style={{ marginTop: 15 }}>
                            <Text style={{ fontFamily: 'Nunito-Bold', color: '#313131' }}>
                                1.192 suka
                            </Text>
                        </View>
                        <View>
                            <Text style={{ fontSize: 12, textAlign: 'justify', marginTop: 20, fontFamily: 'Nunito-Light' }}>
                                Assalammu’alaikum Wr. Wb. {`\n`}
                                Dengan Rasa syukur kami akan mengadakan acara yasinan rutinan yang akan diselenggarakan oleh Pondok Pesantren Darul Hikmah Semarang, berikut detail pelaksanaanya :  {`\n`}

                                Pondok Pesantren : Darul Hikmah Semarang  {`\n`}
                                Tanggal : 12 November 2020  {`\n`}
                                Jam : 19:30 sampai selesai  {`\n`}
                                Santri : 30 santri  {`\n`}
                                Kuota arwahan : 23 / 100  {`\n`}
                                Infaq : 20.000 / arwah  {`\n`}
                                Alamat : jl. Imam bonjol no 201 semarang  {`\n`}
                                Anak Yatim : Yayasan yatim piatu semarang  {`\n`}

                                Daftar Acara :  {`\n`}
                                1. Pembukaan  {`\n`}
                                2. Fadilah/ pembacaan nama-nama arwah  {`\n`}
                                3. yasinan & tahlil  {`\n`}
                                4. Doa  {`\n`}
                                5. Penutup {`\n`}

                                Dana yang terkumpul sebagian akan kami sedekahkan kepada yatim piatu yang telah ditentukan.
                                Mari bersedekah dan mendoakan atas nama orang yang kita sayangi yang sudah tiada.
                                Wassalammu’alaikum Wr. Wb.
                            </Text>
                        </View>
                        <TouchableOpacity onPress={() => {
                            navigation.navigate('ListDetail')
                        }} style={{ width: width * 0.8, height: 46, backgroundColor: '#0D7367', borderRadius: 23, marginVertical: 20, justifyContent: "center", alignItems: 'center', alignSelf: 'center', }}>
                            <Text style={{ color: 'white', fontFamily: 'Nunito-Bold' }}>Lanjutkan</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </>
    )
}
const styles = StyleSheet.create({
    note: { fontFamily: 'Montserrat-Medium', fontSize: 8, color: '#646464', marginHorizontal: 7 }
})