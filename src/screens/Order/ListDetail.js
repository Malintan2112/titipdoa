import React, { useEffect, useState, useRef } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    TouchableOpacity,
    Image,
    Dimensions,
    ActivityIndicator,
    Animated
} from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import * as Animatable from 'react-native-animatable';
import HeaderItems from '../../components/Header';
import { Content, Input } from 'native-base';

import ImageLoader from "../../helpers/ImagesLoader";
import { Card } from 'native-base';
const { width, height } = Dimensions.get('window');
import RBSheet from "react-native-raw-bottom-sheet";


export default ListDetail = ({ navigation, route }) => {
    const [scrollY] = useState(new Animated.Value(0))
    const [form, setForm] = useState({});
    const [formStatus, setFormStatus] = useState(false);

    const opacity = scrollY.interpolate({
        inputRange: [0, 30],
        outputRange: [1, 0],
        extrapolate: 'clamp'

    })

    const refRBSheet = useRef();

    const [listName, setListName] = useState(
        [
            { name: 'Nanang Kurnia Wuhub', date_die: '17 Agustus 1945', weton: 'Jumat Kliwon', selected: false, price: 'Rp 20.000' },
            { name: 'Nanang Kurnia Wuhub', date_die: '17 Agustus 1945', weton: 'Jumat Kliwon', selected: true, price: 'Rp 20.000' }
        ]
    )
    const rbSheetContent = () => (
        <RBSheet
            ref={refRBSheet}
            onClose={(ref) => {

            }}

            openDuration={100}
            animationType={'slide'}
            dragFromTopOnly={true}
            customStyles={{
                container: {
                    paddingHorizontal: width * 0.08,
                    paddingVertical: width * 0.08,
                    borderTopRightRadius: 30,
                    borderTopLeftRadius: 30,
                    height: 380
                }
            }}
        >

            <View>
                <Text style={{ fontFamily: 'Nunito-Bold' }}>Tambah Daftar Almarhum</Text>
            </View>
            <View style={{ marginTop: 20, marginBottom: 10 }}>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    Nama
                </Text>
                <View style={{ width: '100%', height: 40, borderBottomColor: '#747474', borderBottomWidth: 1 }}>
                    <Input
                        selectionColor="#2C2E34"
                        onChangeText={(text) => setForm({ ...form, name: text })}
                        style={{ color: 'black', width: '70%', fontSize: 16 }} numberOfLines={1} ellipsizeMode="tail"
                        value={form.name}
                        placeholderTextColor="#747474"
                        placeholder="Masukkan Nama"
                        keyboardType='default'
                    />
                </View>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    Tanggal Meninggal
                </Text>
                <View style={{ width: '100%', height: 40, borderBottomColor: '#747474', borderBottomWidth: 1 }}>
                    <Input
                        selectionColor="#2C2E34"
                        onChangeText={(text) => setForm({ ...form, date_die: text })}
                        style={{ color: 'black', width: '70%', fontSize: 16 }} numberOfLines={1} ellipsizeMode="tail"
                        value={form.date_die}
                        placeholderTextColor="#747474"
                        placeholder="dd/mm/yyyy"
                        keyboardType='default'
                    />
                </View>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                    Hari Weton Meninggal <Text style={{ fontSize: 10 }}>( Optional )</Text>
                </Text>
                <View style={{ width: '100%', height: 40, borderBottomColor: '#747474', borderBottomWidth: 1 }}>
                    <Input
                        selectionColor="#2C2E34"
                        onChangeText={(text) => setForm({ ...form, weton: text })}
                        style={{ color: 'black', width: '70%', fontSize: 16 }} numberOfLines={1} ellipsizeMode="tail"
                        value={form.weton}
                        placeholderTextColor="#747474"
                        placeholder="Masukan Weton "
                        keyboardType='default'
                    />
                </View>

            </View>
            <TouchableOpacity onPress={() => {
                const reForm = [...listName];
                reForm.push({ name: form.name, date_die: form.date_die, weton: form.weton, selected: true, price: 'Rp 20.000' })
                setListName(reForm)
                setForm({})
                refRBSheet.current.close()
            }} style={{ width: width * 0.8, height: 46, backgroundColor: '#0D7367', borderRadius: 23, marginTop: 20, justifyContent: "center", alignItems: 'center', alignSelf: 'center' }}>
                <Text style={{ color: 'white' }}>Simpan Nama</Text>
            </TouchableOpacity>
        </RBSheet>
    )
    return (
        <>
            {rbSheetContent()}
            <HeaderItems back={true} title='Almarhum' navigation={navigation} />
            <ScrollView onScroll={Animated.event([{ nativeEvent: { contentOffset: { y: scrollY } } }], { useNativeDriver: false })}>
                <View style={styles.MainContainer}>
                    <View style={{ width: '100%', height: 130, backgroundColor: '#F0F0F0', borderRadius: 10, borderColor: '#DCDCDC', borderWidth: 1, marginBottom: 30, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 10 }}>

                        <Image source={{ uri: 'https://tni-au.mil.id/konten/unggahan/2019/04/Sambut_Bulan_Ramadhan__1_.jpg' }} style={{ width: '40%', height: 99, borderRadius: 10, marginRight: 10 }} />
                        <View>
                            <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 16 }}>Yasinan</Text>
                            <Text style={{ fontSize: 12 }}>Darul Hikmah Semarang</Text>
                            <Text style={{ fontSize: 12 }}>12 November 2020</Text>
                            <Text style={{ fontSize: 12 }}>30 Santri</Text>

                        </View>
                    </View>
                    {formStatus ? (
                        <View>
                            <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 15 }}>Rekap Infaq</Text>

                            {listName.map((data, index) => (
                                <View key={index} style={{ width: '100%', marginTop: 10, borderBottomColor: '#EFEFEF', minHeight: 50, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                                    <View style={{ width: '80%' }}>
                                        <Text style={{ fontSize: 12, fontFamily: 'Nunito-SemiBold' }}>{data.name}</Text>
                                    </View>
                                    <TouchableOpacity style={{ height: 30 }}>
                                        <Text style={{ fontSize: 11, color: '#747474' }}>{data.price}</Text>

                                    </TouchableOpacity>
                                </View>
                            ))}
                            <TouchableOpacity onPress={() => refRBSheet.current.open()} style={{ marginTop: 20, flexDirection: 'row', alignItems: "center", justifyContent: 'space-between' }}>
                                <Text style={{ fontSize: 17, fontFamily: 'Nunito-Bold' }}> Total Infaq </Text>
                                <Text style={{ fontSize: 17, fontFamily: 'Nunito-SemiBold', color: '#0D7367' }}>Rp. 80.000</Text>

                            </TouchableOpacity>
                        </View>
                    ) : (
                            <View>
                                <Text style={{ fontFamily: 'Nunito-Bold', fontSize: 15 }}>Daftar Nama Almarhum</Text>

                                {listName.map((data, index) => (
                                    <View key={index} style={{ width: '100%', marginTop: 10, borderBottomColor: '#EFEFEF', borderBottomWidth: 1.2, minHeight: 70, justifyContent: 'space-between', flexDirection: 'row', alignItems: 'center' }}>
                                        <View style={{ width: '80%' }}>
                                            <Text style={{ fontSize: 12, fontFamily: 'Nunito-SemiBold' }}>{data.name}</Text>
                                            <Text style={{ fontSize: 11, color: '#747474' }}>{data.weton}, {data.date_die}</Text>
                                        </View>
                                        <TouchableOpacity onPress={() => {
                                            const tempList = [...listName];
                                            tempList[index].selected = !data.selected;
                                            setListName(tempList)
                                        }} style={{ width: 30, height: 30 }}>
                                            <Image source={ImageLoader(data.selected ? 'check_box_aktive' : 'check_box_non')} style={{ width: 20, height: 20 }} />
                                        </TouchableOpacity>
                                    </View>
                                ))}
                                <TouchableOpacity onPress={() => refRBSheet.current.open()} style={{ marginTop: 20, flexDirection: 'row', alignItems: "center", }}>
                                    <Image source={ImageLoader('add_circle')} style={{ width: 20, height: 20, marginRight: 10 }} />
                                    <Text style={{ fontSize: 12, fontFamily: 'Nunito-Bold' }}> Tambahkan Nama</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                    <TouchableOpacity onPress={() => setFormStatus(true)} style={{ width: width * 0.8, height: 46, backgroundColor: '#0D7367', borderRadius: 23, marginTop: 30, justifyContent: "center", alignItems: 'center', alignSelf: 'center' }}>
                        <Text style={{ color: 'white' }}>Lanjutkan</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </>
    )
}
const styles = StyleSheet.create({
    MainContainer: { width, minHeight: height, backgroundColor: 'white', paddingHorizontal: width * 0.07, paddingVertical: width * 0.08 },
});