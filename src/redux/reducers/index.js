import { combineReducers } from "redux";
import { reducerMain } from "./ReducerMain";
const rootReducer = combineReducers({ reducerMain });
export default rootReducer;