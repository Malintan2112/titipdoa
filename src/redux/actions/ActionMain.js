export const types = {
    SET_USER_NAME: 'SET_USER_NAME',
    SET_API_TOKEN: 'SET_API_TOKEN'
}
export const actionCreatorMain = {
    set_user_name: item => ({ type: types.SET_USER_NAME, payload: item }),
    set_api_token: item => ({ type: types.SET_API_TOKEN, payload: item }),
}
